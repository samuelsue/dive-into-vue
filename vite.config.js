import {createVuePlugin} from 'vite-plugin-vue2';
import {defineConfig } from 'vite';
export default defineConfig({
  resolve: {
    alias: [
      { find: 'vue', replacement: 'vue/dist/vue.esm' }
    ]
  },
  plugins:[createVuePlugin()]
})