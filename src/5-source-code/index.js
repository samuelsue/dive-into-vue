import h from "./m_snabbdom/h"
import patch from "./m_snabbdom/patch";

const container = document.querySelector('#app')
const vnode0 = h('div', { key: 123 }, [
  h('h3', { key: 'A' }, 'A'),
  h('h3', { key: 'B' }, 'B'),
  h('h3', { key: 'C' }, 'C'),
  h('h3', { key: 'D' }, 'D'),
  h('h3', { key: 'E' }, 'E'),
])
patch(container, vnode0)

/*
// test reverse顺序
const vnode1 = h('div', { key: 123 }, [
  h('h3', { key: 'E' }, 'E'),
  h('h3', { key: 'D' }, 'D'),
  h('h3', { key: 'C' }, 'Raw-C'),
  h('h3', { key: 'B' }, 'B'),
  h('h3', { key: 'A' }, 'Raw-A'),
]) */

/* // 测试新结点插入的情况
const vnode1 = h('div', { key: 123 }, [
  h('h2', { key: 'HEHE' }, 'HEHEHEH'),
  h('h3', { key: 'A' }, 'A'),
  h('h3', { key: 'B' }, 'B'),
  h('h3', { key: 'C' }, 'C'),
  h('h3', { key: 'QQ' }, 'QQ'),
  h('h3', { key: 'D' }, 'D'),
  h('h3', { key: 'E' }, 'E'),
  h('h3', { key: 'GG' }, 'GG'),
]) */


// 测试删除结点的情况
const vnode1 = h('div', { key: 123 }, [
  h('h3', { key: 'QQ' }, 'QQ'),
  h('h3', { key: 'C' }, 'C'),
  h('h3', { key: 'B' }, 'B'),
  h('h3', { key: 'A' }, 'A'),
  h('h2', { key: 'HEHE' }, 'HEHEHEH'),
])


document.querySelector('button#btn').onclick = () => {
  patch(vnode0, vnode1)
}