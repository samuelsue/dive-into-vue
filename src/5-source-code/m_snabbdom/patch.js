import { createElement, hasChildren } from "./createElement";
import patchVNode from "./patchVNode";
import vnode from "./vnode";

export const isSameVNode = (node1, node2) => node1.key === node2.key && node1.sel === node2.sel

export default function patch(oldVNode, newVNode) {
  // 判断old是否是VNode
  if (!oldVNode.sel) {
    // 如果是DOM结点
    oldVNode = vnode(oldVNode.tagName.toLowerCase(), {}, [], undefined, oldVNode);
  }
  if (isSameVNode(oldVNode, newVNode)) {
    patchVNode(oldVNode, newVNode)
  } else {
    // 不是同一个VNode，暴力干掉旧的，插入新的
    const newElm = createElement(newVNode)
    oldVNode.elm.parentNode.insertBefore(newElm, oldVNode.elm)
  }
}