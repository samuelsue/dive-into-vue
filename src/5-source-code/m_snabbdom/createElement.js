export const isTextVNode = (node) => !!node.text
export const hasChildren = (node) => Array.isArray(node.children) && !!node.children.length

export function createElement(vnode) {
  const domNode = document.createElement(vnode.sel);
  if (isTextVNode(vnode)) {
    // 纯文本结点
    domNode.innerText = vnode.text
  } else if(hasChildren(vnode)) {
    vnode.children.forEach(child => {
      domNode.appendChild(createElement(child))
    })
  }
  vnode.elm = domNode;
  return vnode.elm;
}