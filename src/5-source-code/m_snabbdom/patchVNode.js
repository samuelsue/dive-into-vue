import { hasChildren, createElement } from "./createElement"
import updateChildren from "./updateChildren"

export default function patchVNode(oldVNode, newVNode) {
  // 同一个VNode结点
  if (oldVNode === newVNode) return
  // newVNode有text
  if (newVNode.text && !hasChildren(newVNode)) {
    // old和new的text不同
    if (oldVNode.text !== newVNode.text) {
      oldVNode.elm.innerText = newVNode.text
    }
  } else {
    // newVNode没有text(有children)
    // oldVNode是个纯text结点
    if (!hasChildren(oldVNode)) {
      oldVNode.elm.innerText = '' // 清空old
      for (const vnode of newVNode.children) {
        oldVNode.elm.appendChild(createElement(vnode))  // 将new的children插入到old中
      }
    } else {
      // oldVNode有children
      updateChildren(oldVNode.elm, oldVNode.children, newVNode.children);
    }
  }
}