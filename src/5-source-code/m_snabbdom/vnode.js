export default function (sel, data, children, text, elm) {
  const vnode = {
    sel,
    data,
    children,
    text,
    elm
  }
  if (data) {
    // 将vnode.data的属性，get到vnode上
    Object.keys(data).forEach(key => {
      Object.defineProperty(vnode, key, {
        get() {
          return this.data[key]
        }
      })
    })
  }
  return vnode
}