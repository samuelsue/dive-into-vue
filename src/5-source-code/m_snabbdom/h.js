import VNode from './vnode'

const isPrmitive = (val) => typeof val === 'string' || typeof val === 'number'
const isVnode = (val) => typeof val === 'object' && val.hasOwnProperty('sel')

/**
 * 精简版的h函数， 接收三个参数
 * @param sel 选择器
 * @param data 属性数据
 * @param children 子vnode，可以是vnode(数组)或者字符串或者数字
 */
export default function h(sel, data, children) {
  if (arguments.length !== 3) {
    throw new Error("h function must take 3 arguments!")
  }
  // 检查children类型
  if (isPrmitive(children)) {
    return VNode(sel, data, undefined, `${children}`, undefined)
  }
  if (Array.isArray(children)) {
    const c = []
    for (const child of children) {
      if (!isVnode(child)) {
        throw new Error('children must be Vnode')
      }
      c.push(child)
    }
    return VNode(sel, data, c, undefined, undefined)
  }
  if (isVnode(children)) {
    return VNode(sel, data, [children],undefined,undefined)
  }
}