import {
  autorun,
  observe
} from "./utils"

const state = {
  count: 0,
  subState: {
    countSub: -1
  }
}
observe(state) // 定义监听

autorun(() => {
  // 此处读取了state.getter触发了依赖收集
  console.log(state.count)
});
autorun(() => {
  console.log("sub:", state.subState.countSub);
})
state.count++;
setTimeout(() => state.subState.countSub--, 1000);