import Vue from 'vue';
import { RulesPlugin } from './simple-plugin';

Vue.use(RulesPlugin);


const vm = new Vue({
  data: { foo: 10 },
  rules: {
    foo: {
      validate: value => value > 1,
      message: 'foo must be greater than one'
    }
  }
})

vm.foo = 0 // should log: "foo must be greater than one"