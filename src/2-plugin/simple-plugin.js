export const RulesPlugin = {
  install(Vue) {
    Vue.mixin({
      created() {
        // 这里可以读取到所有的传递属性this.$options
        if (this.$options.rules) {
          Object.keys(this.$options.rules).forEach(key => {
            const { validate, message } = this.$options.rules[key]
            this.$watch(key, (newVal) => {
              if (!validate(newVal)) {
                console.error('Validate Error:', message)
              }
            })
          })
        }
      }
    })
  }
}